name := "Fyber"
version := "1.0"
scalaVersion := "2.12.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
logBuffered in Test := false
mainClass in Compile := Some("fyber.App")
showSuccess := false
