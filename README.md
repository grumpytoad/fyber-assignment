Fyber Assignment 1.

Requires scala version 12.0+. Data file is assumed to be in the classpath.

To run as package:

```
$ sbt package
$ scala target/scala-2.12/fyber_2.12-1.0.jar data_scala.txt
```

To run from sbt:
```
$ sbt --error 'run [filename]'
```

To run the tests:

```
$ sbt test
```
