package fyber

import io.Source
import org.scalatest.FlatSpec

class AppTest extends FlatSpec {

  "A countAggregator" should "count correctly" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1 :: 2 :: 3 :: 2 :: 3 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.countAggregator).toList
    }
  }

  "A valueAggregator" should "return the value at each data point" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1.80215 :: 1.80185 :: 1.80195 :: 1.80225 :: 1.80215 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.valueAggregator).toList
    }
  }

  "A keyAggregator" should "return the number of seconds since Epoch at each data point" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1355270609 :: 1355270621 :: 1355270646 :: 1355270702 :: 1355270702 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.keyAggregator).toList
    }
  }

  "A rollingSumAggregator" should "return the rolling sum of data point values inside the 60 second window" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1.80215 :: 3.604 :: 5.40595 :: 3.6042 :: 5.40635 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.rollingSumAggregator).toList map App.roundDouble
    }
  }

  "A minValueAggregator" should "return the minimum value in data points within the 60 second window" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1.80215 :: 1.80185 :: 1.80185 :: 1.80195 :: 1.80195 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.minValueAggregator).toList map App.roundDouble
    }
  }


  "A maxValueAggregator" should "return the maximum value in data points within the 60 second window" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = 1.80215 :: 1.80215 :: 1.80215 :: 1.80225 :: 1.80225 :: Nil
    assertResult(expected) {
      App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.maxValueAggregator).toList map App.roundDouble
    }
  }

  "Composing streams" should "return multiple streams in parallel" in {
    val points = App.toPoints(Source.fromResource("data_scala.txt").getLines).slice(0, 5)
    val expected = List(
        1355270609 :: 1.80215 :: 1 :: 1.80215 :: 1.80215 :: 1.80215 :: Nil,
        1355270621 :: 1.80185 :: 2 :: 3.604 :: 1.80185 :: 1.80215 :: Nil,
        1355270646 :: 1.80195 :: 3 :: 5.40595 :: 1.80185 :: 1.80215 :: Nil,
        1355270702 :: 1.80225 :: 2 :: 3.6042 :: 1.80195 :: 1.80225 :: Nil,
        1355270702 :: 1.80215 :: 3 :: 5.40635 :: 1.80195 :: 1.80225 :: Nil
    )
    assertResult(expected) {
      (App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.countAggregator) zip
       App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.valueAggregator) zip
       App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.keyAggregator) zip
       App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.rollingSumAggregator) zip
       App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.minValueAggregator) zip
       App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.maxValueAggregator)).map
        { case (((((c, v), k), r), mn), mx) => k :: v :: c :: App.roundDouble(r) :: App.roundDouble(mn) :: App.roundDouble(mx) :: Nil }.toList
    }
  }
}
