package fyber

import io.Source

sealed trait TimedData {
  val timestamp: Long
}

case class DataPoint (timestamp: Long, priceRatio: Double) extends TimedData

case class Window(sizeMs: Long)

case class Aggregator[-P, T] (initializer: T, aggregate: (P, T) => T)

object App {
  def countAggregator = Aggregator[Any, Long] (0, (_, xs) => xs + 1)
  def valueAggregator = Aggregator[DataPoint, Double] (0.0, (dataPoint, _) => dataPoint.priceRatio)
  def keyAggregator = Aggregator[DataPoint, Long] (0, (dataPoint, _) => dataPoint.timestamp)
  def rollingSumAggregator = Aggregator[DataPoint, Double] (0.0, (dataPoint, xs) => xs + dataPoint.priceRatio)
  def minValueAggregator = Aggregator[DataPoint, Double] (Double.MaxValue, (dataPoint, xs) => xs.min(dataPoint.priceRatio))
  def maxValueAggregator = Aggregator[DataPoint, Double] (Double.MinValue, (dataPoint, xs) => xs.max(dataPoint.priceRatio))
  def header = "T" :: "V" :: "N" :: "RS" :: "MinV" :: "MaxV" :: Nil

  def main (args: Array[String]) {
    args.headOption match {
      case Some(inputFile) =>
        Option(getClass.getClassLoader.getResource(inputFile)) match {
          case Some(url) =>
            val points = App.toPoints(Source.fromInputStream(url.openStream).getLines)
            val resStream = (App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.countAggregator) zip
                App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.valueAggregator) zip
                App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.keyAggregator) zip
                App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.rollingSumAggregator) zip
                App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.minValueAggregator) zip
                App.byWindow(points drop 1, points.head #:: Stream.empty, Window(60), App.maxValueAggregator)).map
            { case (((((c, v), k), r), mn), mx) => k :: v :: c :: App.roundDouble(r) :: App.roundDouble(mn) :: App.roundDouble(mx) :: Nil }
            val formatSpec = "%-11s%-8s%-2s%-8s%-8s%-8s"
            println(formatSpec.format(header:_*))
            resStream.foreach{ v =>
              println(formatSpec.format(v:_*))
            }
          case _ => println ("Missing file (not in classpath): " + inputFile)
        }
      case _ =>
        println ("Usage: scala fyber.jar [filename]")
    }
  }

  // note: will crash if it's not the right format
  def toPoints(lines: Iterator[String]): Stream[DataPoint] = lines.map {
    line => line.split("\t") match {
      case Array(timestamp, priceRatio) => DataPoint(timestamp.toLong, priceRatio.toDouble)
      case _ => throw new IllegalArgumentException("Badly formatted data: " + line)
    }
  }.toStream

  def roundDouble(d: Double) = "%.5f".format(d).toDouble

  def byWindow[T, P <: TimedData](points: Stream[P], grouped: Stream[P], window: Window, aggregator: Aggregator[P, T]): Stream[T] = points match {
    case first #:: rest if first.timestamp <= grouped.last.timestamp + window.sizeMs =>
      (grouped.foldRight(aggregator.initializer)(aggregator.aggregate)) #:: byWindow(rest, first #:: grouped,  window, aggregator)
    case first #:: rest if first.timestamp > grouped.last.timestamp =>
      (grouped.foldRight(aggregator.initializer)(aggregator.aggregate)) #:: byWindow(rest, first #:: grouped.filter(g => first.timestamp <= g.timestamp + window.sizeMs), window, aggregator)
    case Stream.Empty => grouped.foldRight(aggregator.initializer)(aggregator.aggregate) #:: Stream.empty
  }
}
